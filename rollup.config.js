//yarn add --dev rollup rollup-plugin-babel rollup-plugin-node-resolve rollup-plugin-peer-deps-external rollup-plugin-commonjs @svgr/rollup rollup-plugin-url
/*
rollup-plugin-babel: rollup에서 babel 을 사용 할 수 있게 해주는 플러그인입니다.
rollup-plugin-node-resolve: node_modules에서 써드파티 모듈을 사용하는 용도로 사용하며, js 이외의 확장자 (ts, tsx) 파일을 불러오기 위해서도 이 플러그인을 필요로 합니다.
rollup-plugin-peer-deps-external: peerDependency로 설치된 라이브러리의 코드가 번들링된 결과에 포함되지 않고, import 구문으로 불러와서 사용할 수 있게 만들어줍니다.
rollup-plugin-commonjs: CommonJS 형태로 이루어진 모듈의 코드를 ES6로 변환하여 결과물에 포함될 수 있게 해줍니다. (현재 우리 프로젝트에서는 필수로 적용해야하는 플러그인은 아닙니다)
@svgr/rollup: SVG를 컴포넌트 형태로 불러와서 사용 할 수 있게 해줍니다.
rollup-plugin-url: data-URI형태로 svg, png, jpg 파일 등을 불러와서 사용 할 수 있게 해줍니다. @svgr/rollup 플러그인을 사용 할 때, rollup-plugin-url과 함께 사용을 해야만 import { ReactComponent as icon } from './icon.svg' 형태의 코드를 사용 할 수 있습니다.
peerDependency 설정하기
ex) yarn add --peer react react-dom
*/
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import pkg from './package.json';
import svgr from '@svgr/rollup';
import url from 'rollup-plugin-url';
import mdx from 'rollup-plugin-mdx';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import less from 'rollup-plugin-less';
import postcss from 'rollup-plugin-postcss';

// babel-preset-react-app를 사용한다면 BABEL_ENV를 필수로 설정해야함.
process.env.BABEL_ENV = 'development';

export default {
    input: 'stories/index.js', // 어떤 파일부터 불러올지 정함.
    output: [
        {
            file: pkg.main, // 번들링한 파일을 저장 할 경로
            format: 'es', // ES Module 형태로 번들링함
        },
    ],
    plugins: [
        peerDepsExternal(),
        mdx({
            extensions: ['.md', '.mdx'],
        }),
        babel({ extensions: ['.mjs', '.js', '.jsx', '.md'], include: ['stories/**/*'], runtimeHelpers: true }), // Babel을 사용 할 수 있게 해줌
        resolve({ extensions: ['.mjs', '.js', '.jsx', '.json', '.md'] }),
        commonjs({
            include: 'node_modules/**',
            // namedExports: {
            //     'node_modules/schema-typed/es/index.js': ['SchemaModel', 'StringType', 'NumberType', 'ArrayType', 'DateType', 'ObjectType','BooleanType'],
            //     'node_modules/react-virtualized/dist/commonjs/CellMeasurer/index.js': ['CellMeasurerCache', 'CellMeasurer']
            // }
        }), // CommonJS 형태로 만들어진 모듈도 불러와서 사용 할 수 있게 해줌. 현재 프로젝트 상황에서는 없어도 무방함
        url(), // 미디어 파일을 dataURI 형태로 불러와서 사용 할 수 있게 해줌.
        svgr(), // SVG를 컴포넌트로 사용 할 수 있게 해줌.
        postcss({
            extensions: ['.css', '.scss', '.less'],
            use: ['sass', ['less', { javascriptEnabled: true }]],
        }),
        less({
            insert: true,
        }),
    ],
    external: [...Object.keys(pkg.peerDependencies || {})],
};
