# season2.uikit

`설치`
```
npm i -g yalc && yarn
```
`개발용 로컬서버 띄우기`
```
yarn storybook
```

`공용모듈 배포(최초)`
yalc에 설치 모듈임을 등록함(최초 한번)
```
yalc publish
```
`프로젝트에서 popkontv.season2.uikit 추가(최초)`
yalc에서 가져와서 신규/기존 프로젝트에 연결(최초 한번)
yalc 오류로 컴포넌트 추가될 때마다 필요 프로젝트에서 yalc add작업 필요함
```
yalc add popkontv.season2.uikit
```
`공용모듈 수정 후 배포(반복)`
```
yarn build && yalc push
```


