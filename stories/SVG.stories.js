import React,{useCallback} from 'react';
import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';

import mdx from './SVG.mdx';



export default {
    title: 'SVG',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    root:{

    }
});

export const BasicSVG = (props)=>{


    const classes = useStyles();


    return(
        <svg className={classNames([classes.root,props.styles])}  {...props}/>
    )
}


