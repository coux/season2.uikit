import React,{useCallback} from 'react';
import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';

import { Calendar,Whisper,Popover,Badge,IntlProvider } from 'rsuite';


import mdx from './Calendar.mdx';


export default {
    title: 'Calendar',
     parameters: {
         docs: {
             page: mdx
         }
     }
};

const useStyles = makeStyles({
    root:{

    }
});


export const BasicCalendar = (props)=>{


    const classes = useStyles();


    return(

            <Calendar bordered className={classNames([classes.root,props.styles])}  {...props} />

    )
}

