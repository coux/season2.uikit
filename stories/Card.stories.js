import { Card, CardContent, makeStyles } from '@material-ui/core';
import React from 'react';
import classNames from 'classnames';

export default{
    title : 'Card'
}

const useStyles = makeStyles({
    root:{
        background: '#f1f4f9',
        boxShadow:'none',
        border: '1px solid #e3e9f3'
    },
    close:{
        background: '#FF0000',
        border: '1px solid #FF0000'
    }
});

export const BasicCard = (props) =>{
    const classes = useStyles();

    return(
        <Card className={classNames(props.styles,classes.root)}>
            <CardContent {...props}/>
        </Card>
    )
}

export const CardEx = (props) =>(
    <BasicCard>
        <p>
            필요한 팝콘 : 500개
        </p>
        <p>
            현재 내 팝콘 : 0개
        </p>
    </BasicCard>
)

export const ClosableCard = (props) =>{
    const classes = useStyles();

    return(
        <BasicCard styles={classes.close}/>
    )
}