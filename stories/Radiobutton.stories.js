import { FormControl, FormControlLabel, FormGroup, Radio, RadioGroup } from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import mdx from './Radiobutton.mdx';

export default {
    title: '/selectionControls/Radiobutton',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

export const BasicRadioButton = (props) => {
    const [value, setValue] = useState('');

    const handleChange = useCallback((e) => {
        setValue(e.target.value);
        console.log('select', e.target.value);
    }, []);

    return (
        <FormControl>
            <RadioGroup defaultValue={props.defaultValue} onChange={handleChange}>
                {
                    props.list &&
                    props.list.map((v, idx) => {
                        return (
                            <FormControlLabel value={v.value} key={idx}
                                control={<Radio color="primary" />}
                                label={v.label} labelPlacement="bottom" />
                        )
                    })
                }

            </RadioGroup>
        </FormControl>
    )
}

// Radiobutton Props 정의
BasicRadioButton.propTypes = {
    /** true일 경우 컴포넌트는 체크된 상태 */
    checked: PropTypes.bool,
    /** 체크된 상태일때의 표현하는 아이콘 */
    checkedIcon: PropTypes.node,
    /** 컴포넌트의 색상 ({ 'default' | 'primary' | 'secondary' }) */
    color: PropTypes.string,
    /** true일 경우 스위치가 비활성화 */
    disabled: PropTypes.bool,
    /** 인풋의 이름 엘리먼트 */
    name: PropTypes.string,
    /** 상태가 변경되면 콜백하는 함수 */
    onChange: PropTypes.func,
    /** true일 경우 input요소가 필요함  */
    required: PropTypes.bool,
    /** 체크박스의 크기  ({ 'medium'| 'small' }) */
    size: PropTypes.string,
    /** 구성요소의 값 */
    value: PropTypes.any,
}

BasicRadioButton.defaultProps = {
    size: 'medium'
}