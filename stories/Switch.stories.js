import React, { useState } from 'react';
import mdx from './Switch.mdx';
import PropTypes from 'prop-types';
import { useCallback } from 'react';
import { FormControlLabel, Switch } from '@material-ui/core';
import { BasicTheme } from './BasicTheme.stories';

export default{
    title: '/selectionControls/Switch',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

export const BasicSwitch = (props) => {

    const [checked, setChecked] = useState(props.checked? props.checked : false);
    const onHandleChange = useCallback(()=>{
        setChecked(prev=>!prev);
    },[]);

    return (
        <BasicTheme>
            <FormControlLabel 
                label={props.label ? props.label : 'label'}
                labelPlacement={props.labelPlacement}
                control={<Switch color={props.color? props.color : 'primary'} checked={checked}
                onChange={onHandleChange} {...props} />}
            />
        </BasicTheme>        
    )
}

BasicSwitch.propTypes = {
    /** true일 경우 컴포넌트는 체크된 상태 */
    checked: PropTypes.bool,
    /** 체크된 상태일때의 표현하는 아이콘 */
    checkedIcon: PropTypes.node,
    /** 컴포넌트의 색상 ({ 'default' | 'primary' | 'secondary' }) */
    color: PropTypes.string,
    /** true일 경우 스위치가 비활성화 */
    disabled: PropTypes.bool,
    /** 값에 따라 한쪽의 패딩이 변경됨  ({ 'end' | 'start' | false })  */
    edge: PropTypes.string,
    /** 상태가 변경되면 콜백하는 함수 */
    label: PropTypes.string,
    /** 상태가 변경되면 콜백하는 함수 */
    labelPlacement: PropTypes.string,
    /** 상태가 변경되면 콜백하는 함수 */
    onChange: PropTypes.func,
    /** true일 경우 input요소가 필요함  */
    required: PropTypes.bool,
    /** 스위치의 크기  */
    size: PropTypes.string,
    /** 구성요소의 값 */
    value: PropTypes.any,
}

BasicSwitch.defaultProps = {
    color: 'primary',
    size : 'medium'
}