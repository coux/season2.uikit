import React, { useCallback } from 'react';
import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';

import { DatePicker as TestPicker, Dropdown } from 'rsuite';
import koLocale from 'date-fns/locale/ko';

import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
// import './index.less';
// import 'rsuite/lib/styles/themes/dark/index.less';
import 'date-fns';

import DateFnsUtils from '@date-io/date-fns';

import mdx from './DatePicker.mdx';

export default {
    title: 'DatePicker',
    parameters: {
        docs: {
            page: mdx,
        },
    },
};

const useStyles = makeStyles({
    root: {},
});

export const BasicDatePicker = (props) => {
    const classes = useStyles();

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={koLocale}>
            <DatePicker okLabel="확인" cancelLabel="취소" className={classNames([classes.root, props.styles])} {...props} theme="dark" />
        </MuiPickersUtilsProvider>
    );
};

export const TestDatePicker = (props) => <TestPicker />;

export const TestDropdown = (props) => (
    <Dropdown title="Default">
        <Dropdown.Item>New File</Dropdown.Item>
        <Dropdown.Item>New File with Current Profile</Dropdown.Item>
        <Dropdown.Item>Download As...</Dropdown.Item>
        <Dropdown.Item>Export PDF</Dropdown.Item>
        <Dropdown.Item>Export HTML</Dropdown.Item>
        <Dropdown.Item>Settings</Dropdown.Item>
        <Dropdown.Item>About</Dropdown.Item>
    </Dropdown>
);
