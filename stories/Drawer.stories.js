import React,{useCallback} from 'react';
import { Drawer,Button,List  } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';

import mdx from './Drawer.mdx';



export default {
    title: 'Drawer',
   parameters: {
       docs: {
           page: mdx
       }
   }
};

const useStyles = makeStyles({
    root:{

    }
});

export const BasicDrawer = (props)=>{


    const classes = useStyles();


    return(
        <Drawer  className={classNames([classes.root,props.styles])}  {...props}/>
    )
}


