import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, makeStyles } from '@material-ui/core';

export default{
    title: 'Dialog'
}

const useStyles = makeStyles({
    buttons : {
        justifyContent:'center'
    }
});

export const BasicDailog = (props) =>{
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    return(
        <Dialog open={open} maxWidth={props.maxWidth} fullWidth={props.fullWidth} {...props} />
    )
}

export const AlertDialog = (props) =>{
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
      };
    
      const handleClose = () => {
        setOpen(false);
      };

    return(
        <BasicDailog>
            <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <div>해당 방송 혹은 방송자를</div>
            <div>신고하시겠습니까?</div> 
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.buttons}>
            <Button onClick={handleClose}>
            아니오
          </Button>
          <Button onClick={handleClose} autoFocus>
            예
          </Button>
            
        </DialogActions>
        </BasicDailog>
    )
}