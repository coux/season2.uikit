import React,{useState,useCallback} from 'react';
import { Tabs, makeStyles } from '@material-ui/core';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import mdx from './Tabs.mdx';
import { BasicTheme } from './BasicTheme.stories';

export default {
    title:'/tabs/Tabs',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    listTab :{
        border : 'solid 1px #dae1e6',
        background : '#f4f4f8'
    },
    listSelected : {
        background:'#fff'
    },
    listIndicator : {
        background : 'none'
    }
});

export const BasicTabs = (props) =>{
    const [value, setValue] = useState(props.value ? props.value : 0);
    const classes = useStyles();
    const handleChange = useCallback((e,newValue)=>{
        setValue(newValue);
    },[setValue]);    
    
    const styles = makeStyles(theme =>({prop:props.styles}));
    return(
        <BasicTheme>
            <Tabs
                value={value}
                indicatorColor={props.indicatorColor}
                centered={props.centered}
                className={classNames(styles().prop)}
                classes={{
                    indicator: props.listflag && classes.listIndicator
                }}
                onChange={handleChange}
                variant={props.variant}
                {...props}
            />
        </BasicTheme>
    )
}

// Tabs Props 정의 
BasicTabs.propTypes = {
    /** 탭이 중앙에 위 치할지 여부 (true | false) */
    centered: PropTypes.bool,
    /** 탭에서 현재 선택 된 값*/
    value : PropTypes.any,
    /** 탭의 추가 표시 동작을 결정  'fullWidth'| 'scrollable'| 'standard'*/
    variant : PropTypes.string, 
    /** 탭의 텍스트 색상 변경 'inherit'| 'primary'| 'secondary'*/
    textColor : PropTypes.string,
    /** 선택된 탭 하단에 색상 표시 'primary'|'secondary'*/
    indicatorColor: PropTypes.string,
    /** 사용자가 커스터마이징하게 줄 수 있는 탭 스타일 */
    styles: PropTypes.object,
}

BasicTabs.defaultProps = {
    centered : false,
    variant: 'standard',
    textColor: 'primary',
    indicatorColor: 'primary'
}

/*
export const TextTabs = (props) =>{
    return(
        <BasicTabs variant="fullWidth" centered={true} {...props}>
            <BasicTab label="방송정보" value={0}/>
            <BasicTab label="시청정보" value={2}/>
            <BasicTab label="test" value={3}/>
        </BasicTabs>
    )
}

export const IconTabs = (props) =>{
    return(
        <BasicTabs variant="scrollable" {...props}>
            <BasicTab icon={<PhoneIcon/>} value={0}/>
            <BasicTab icon={<FavoriteIcon/>} value={1}/>
            <BasicTab icon={<PersonPinIcon/>} value={2}/>
            <BasicTab icon={<HelpIcon/>} value={3}/>
        </BasicTabs>
    )
}

export const ListTabs = (props) =>{
    return(
        <BasicTabs centered={true} listflag="true" {...props}>
            <BasicTab label="랭킹순" value={0} listflag="true"/>
            <BasicTab label="최신순" value={1} listflag="true"/>
            <BasicTab label="시청인원순" value={2} listflag="true"/>
        </BasicTabs>
    )
}
*/