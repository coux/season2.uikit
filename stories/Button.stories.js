import React, { useCallback, useState } from 'react';
import { action } from '@storybook/addon-actions';
import { Avatar, Button, Typography, ButtonBase, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import Zam from '../assets/jam.svg';
import Luxurypopkon from '../assets/luxurypopkon.svg';
import { ReactComponent as Item } from '../assets/item.svg';
import { Settings } from '@material-ui/icons';
import { ReactComponent as Level } from '../assets/level.svg';
/** @jsx jsx */
import { jsx, css } from '@emotion/core';

export default {
    title: 'Button',
    component: Button,
};

const useStyles = makeStyles({
    root: {
        borderRadius: 25,
        fontSize: '14px',
        height: '36px',
    },
    outlined: {
        color: '#222',
        backgroundColor: '#FFF',
    },
    segment: {
        color: '#FFF',
        backgroundColor: '#653ad8',
    },
    setting: {
        color: '#8f9194',
        background: '#fff',
        border: '0.5px solid #d9d9d9',
    },
    ex: {
        width: '500px',
    },
    toggle: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
    },
    text: {
        color: '#888888',
    },
    bold: {
        fontWeight: 'bold',
    },
    button: {
        padding: 0,
    },
    flex: {
        flex: 1,
    },
    minWidth: {
        minWidth: '150px',
    },
    btnMinWidth: {
        minWidth: '130px',
    },
    square: {
        width: '70px',
        height: '70px',
        borderRadius: '8px',
        border: '1px solid #abb4c2',
        backgroundColor: '#fff',
    },
    middle: {
        maxWidth: '100%',
        maxHeight: '100%',
        margin: 'auto',
        display: 'block',
        verticalAlign: 'middle',
    },
    table: {
        display: 'table',
    },
    span: {
        display: 'inline-block',
        verticalAlign: 'middle',
    },
});

// const styles = {
//     level: css`
//         path {
//             fill: yellow;
//         }
//     `,
//     item: css`
//         :hover {
//             filter: grayscale(100%);
//         }
//     `,
// };

// export const Test = (props) => {
//     return <Level css={styles.level}></Level>;
// };

// export const Test2 = (props) => {
//     return <Item css={styles.item} />;
// };

export const BasicButton = (props) => {
    const handleClick = useCallback(() => {
        console.log('click');
        if (props.click) {
            props.click();
        }
    });
    return (
        <Button onClick={handleClick} variant={props.variant} className={classNames(props.styles)} disabled={props.disabled} color={props.color} {...props}>
            {props.label ? props.label : 'BUTTON'}
        </Button>
    );
};

export const ColorButton = (props) => {
    const classes = useStyles();

    return <BasicButton variant="contained" color="primary" styles={classes.root} {...props} />;
};

export const DisabledColorButton = (props) => {
    return <ColorButton disabled={true} {...props} />;
};

export const ColorSquareButton = (props) => {
    return <BasicButton variant="contained" color="primary" {...props} />;
};

export const OutlinedButton = (props) => {
    const classes = useStyles();

    return <BasicButton variant="outlined" color="primary" styles={classes.root} {...props} />;
};

export const SegmentButton = (props) => {
    const classes = useStyles();

    return (
        <ToggleButtonGroup>
            <ToggleButton className={classNames([classes.root, classes.segment])}>{props.firstLabel ? props.firstLabel : 'BUTTON'}</ToggleButton>
            <ToggleButton className={classNames([classes.root, classes.segment])}>{props.secondLabel ? props.secondLabel : 'BUTTON'}</ToggleButton>
        </ToggleButtonGroup>
    );
};

// 텍스트 버튼
export const TextButton = (props) => {
    const classes = useStyles();

    return (
        <Button className={classes.root} onClick={() => action('clicked')}>
            {props.label ? props.label : 'BUTTON'}
        </Button>
    );
};

export const BasicRoundIconButton = (props) => {
    const classes = useStyles();
    console.log('props', props);

    const handleClick = useCallback(() => {
        console.log('click');
        if (props.click) {
            props.click();
        }
        alert('btn Click');
    }, []);

    return <Avatar {...props} className={classNames([classes.setting, props.styles])} onClick={handleClick} {...props}></Avatar>;
};

export const ZamRoundIconButton = (props) => {
    const classes = useStyles();
    return (
        <BasicRoundIconButton {...props}>
            <img src={Zam} />
        </BasicRoundIconButton>
    );
};

/*
export const LuxuryPopkonRoundIconButton = (props) =>{
    return(
        <BasicRoundIconButton {...props}>
            <img src={Luxurypopkon} />
        </BasicRoundIconButton>
    );
};

export const ItemPopkonRoundIconButton = (props) => {
    return (
        <BasicRoundIconButton {...props}>
            <img src={Item} />
        </BasicRoundIconButton>
    )
}
*/
export const SettingRoundIconButton = (props) => {
    console.log('setting props', props);
    return (
        <BasicRoundIconButton {...props}>
            <Settings fontSize="small" />
        </BasicRoundIconButton>
    );
};

export const BasicIconToggleButton = (props) => {
    const classes = useStyles();
    const [flag, setFlag] = useState(false);

    const handleChange = useCallback(() => {
        setFlag((v) => !v);

        if (props.change) {
            props.change();
        }
    }, []);

    return (
        <Button onClick={handleChange} className={classNames([classes.button, props.styles])}>
            <div div className={classes.toggle}>
                <img src={flag ? props.icon : props.unIcon} className={classNames([classes.flex])} />
                <Typography variant="body1" className={classNames([classes.flex, !flag && classes.text])}>
                    {props.text}
                </Typography>
                <Typography variant="button" className={classNames([classes.flex, classes.bold, !flag && classes.text])}>
                    {props.options}
                </Typography>
            </div>
        </Button>
    );
};

export const FollowIconToggleButton = (props) => {
    const classes = useStyles();
    const [text, setText] = useState(props.text ? props.text : '팔로워');
    const [options, setOptions] = useState(props.options ? props.options : '30.1K');
    return <BasicIconToggleButton icon={Follow} unIcon={UnFollow} text={text} options={options} styles={classes.minWidth} {...props} />;
};

/*
export const LikeIconFlagButton = (props) =>{
    const classes = useStyles();
    const [text,setText] = useState(props.text? props.text : '좋아요');
    const [options, setOptions] = useState(props.options ? props.options : '30.1K');
    return(
        <BasicIconToggleButton icon={Like} unIcon={UnLike} text={text} options={options} styles={classes.minWidth} {...props} />
    )
}

export const SubscribeIconFlagButton = (props) =>{
    const classes = useStyles();
    const [text,setText] = useState(props.text? props.text : '구독자');
    const [options, setOptions] = useState(props.options ? props.options : '30.1K');
    return(
        <BasicIconToggleButton icon={Subscribe} unIcon={UnSubscribe} text={text} options={options} styles={classes.minWidth} {...props} />
    )
}
*/

export const BasicIconButton = (props) => {
    const classes = useStyles();

    const handleChange = useCallback(() => {
        if (props.change) {
            props.change();
        }
    }, []);

    return (
        <Button onClick={handleChange} className={classNames([classes.button, props.styles])}>
            <div className={classes.toggle}>
                <img src={props.icon} className={classNames([classes.flex])} />
                <Typography variant="body1" className={classNames([classes.flex, classes.text])}>
                    {props.text}
                </Typography>
            </div>
        </Button>
    );
};

export const DeclareIconButton = (props) => {
    const classes = useStyles();
    const [text, setText] = useState(props.text ? props.text : '신고하기');
    return <BasicIconButton icon={Declare} text={text} styles={classes.btnMinWidth} {...props} />;
};
/*
export const SnsIconButton = (props) =>{
    const classes = useStyles();
    const [text,setText] = useState(props.text? props.text : '공유하기');
    return(
        <BasicIconButton icon={Sns} text={text} styles={classes.btnMinWidth} {...props} />
    )
}
*/

export const BasicSquareButton = (props) => {
    const classes = useStyles();
    return (
        <ButtonBase>
            <Paper className={classNames([classes.square, props.styles])}>
                <span className={classes.span} />
                <img src={Popkon} className={classes.middle} />
                <Typography variant="button">
                    <span className={classes.bold}>10</span>개
                </Typography>
            </Paper>
        </ButtonBase>
    );
};
