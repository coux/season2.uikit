import React,{useCallback} from 'react';
import { Radio, List, ListItem, ListItemText, RadioGroup, ListItemAvatar, Avatar, FormControlLabel   } from '@material-ui/core';
import PropTypes from 'prop-types';
import mdx from './RadioList.mdx';

export default{
    title : 'RadioList',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

export const BasicRadio = (props) =>{
    const [value, setValue] = React.useState('test1');
    const handleChange = (event) => {
        setValue(event.target.value);
    };
    return (
        <List>
            <RadioGroup onChange={handleChange} value={value}>
            {[1, 2, 3].map((q, index) => {
                return (
                    <ListItem key={index}>
                        <ListItemAvatar>
                            <Avatar src=''/>
                        </ListItemAvatar>
                        <ListItemText id={`test${q}`} primary={`test${q}`}  />
                        <FormControlLabel value={`test${q}`} control={<Radio />}  />
                    </ListItem>
                )
            })}
            </RadioGroup>
        </List>
    );
}

// List Props 정의 
BasicRadio.propTypes = {
    /** true이면 선택됨 */
    checked : PropTypes.bool,
    /** 구성 요소를 확인할 때 표시 할 아이콘 */
    checkedIcon : PropTypes.node,
    /** 구성 요소에 적용된 스타일을 재정의 */
    classes : PropTypes.object,
    /** 구성 요소의 색상 */
    color : PropTypes.string,
    /** true이면 목록 항목이 비활성화 */
    disabled : PropTypes.bool,
    /** true이면 잔물결 효과가 비활성화 */
    disableRipple : PropTypes.bool,
    /** 구성 요소가 선택 취소되었을 때 표시되는 아이콘 */
    icon : PropTypes.bool,
    /** 입력 요소의 ID */
    id : PropTypes.string,
    /** 입력 요소에 적용되는 속성 */
    inputProps : PropTypes.object,
    /** 입력 요소에 참조를 전달 */
    inputRef : PropTypes.func,
    /** 입력 요소의 이름 속성 */
    name : PropTypes.string,
    /** 상태가 변경되면 콜백 */
    onChange : PropTypes.func,
    /** true이면 입력 요소가 필요 */
    required : PropTypes.bool,
    /** 라디오의 크기 */
    size : PropTypes.string,
    /** 구성 요소의 값 */
    value : PropTypes.any
}

BasicRadio.defaultProps = {
    color : "secondary",
    size : "medium"
} 

