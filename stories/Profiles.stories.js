import React,{useCallback} from 'react';
import { Avatar, Badge } from '@material-ui/core';
import {AvatarGroup} from '@material-ui/lab';
import Zam from '../assets/jam.svg';
import { Settings } from '@material-ui/icons';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { BasicBadge } from './Badge.stories';
import { SettingRoundIconButton } from './Button.stories';
import ProfileDefault from '../assets/profile_default.jpg';
import CrewProfileDefault from '../assets/crew_profile_default.jpg';

export default{
    title: 'Profile'
}

const useStyles = makeStyles({
    badge: {
        background : '#dd3518'
    },
    root : {
        background : '#fff'
    },
    setting : {
        width : '20px',
        height : '20px'
    },
    live : {
        background : '#dd3518',
        color : '#fff',
        padding:2,
        borderRadius:2,
        fontSize : '2px'
    }
});

export const BasicProfile = (props) =>{
    const handleClick = useCallback(()=>{
        console.log('click');
        if(props.click){
            props.click();
        }
        alert('click');
    },[]);

    return(
        <Avatar onClick={handleClick} variant={props.variant} 
        src={props.src? props.src : (props.crewflag? CrewProfileDefault : ProfileDefault)} {...props}/>
    )
}

export const BasicProfileGroup = (props) =>{
    return(
        <AvatarGroup max={props.max} {...props}/>
    )
}

export const ProfileGroup = (props) =>{
    return(
        <BasicProfileGroup max={4}>
            <BasicProfile >
                <img src={Zam}/>
            </BasicProfile>
            <BasicProfile/>
            <BasicProfile/>
            <BasicProfile/>
            <BasicProfile/>
        </BasicProfileGroup>
    )
}

export const BadgeProfile = (props) =>{
    const classes = useStyles();

    const handleClick = useCallback(()=>{
        console.log('click');
        if(props.click){
            props.click();
        }
        alert('btn Click');
    },[]);

    return(
        <BasicBadge
            overlap="circle"
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}
            onClick={handleClick}
            badgeContent={
                <SettingRoundIconButton styles={classes.setting}/>
            }
        >
            <BasicProfile/>
        </BasicBadge>
    )
}

export const FlagProfile = (props) =>{
    const classes = useStyles();

    return(
        <Badge overlap="circle" variant="dot" classes={{badge:classes.badge}} anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}>
            <BasicProfile crewflag="true"/>
        </Badge>
    )
}

export const LiveFlagProfile = (props) =>{
    const classes = useStyles();
    return(
        <Badge anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
        }}>
            <Avatar />
        </Badge>
    )
}