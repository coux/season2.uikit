import React,{useCallback} from 'react';
import { List, ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction, IconButton } from '@material-ui/core';
import PropTypes, { node } from 'prop-types';
import mdx from './List.mdx';

export default{
    title : 'List',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

export const BasicListItem = (props) =>{
    const handleClick = useCallback(()=>{
        console.log('click', props.idx);
    },[]);

    const handleBtnClick = useCallback(()=>{
        console.log('btnClick', props.idx);
        if(props.btnClick){
            props.btnClick();
        }
    },[]);

    return(
        <ListItem button {...props} onClick={handleClick}>
            {
                props.icon&& 
                <ListItemIcon>
                    <props.icon/>
                </ListItemIcon>
            }
            <ListItemText primary={props.text} />
            {
                props.btn && 
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="comments" onClick={handleBtnClick}>
                        <props.btn/>
                    </IconButton>
                </ListItemSecondaryAction>
            }
        </ListItem>
    )
}

// List Props 정의 
BasicListItem.propTypes = {
    /** align-items 스타일 속성을 정의 */
    alignItems : PropTypes.string,
    /** true 목록 항목이 첫 번째 마운트 중에 초점 */
    autoFocus : PropTypes.bool,
    /** true 목록 항목이 버튼 */
    button : PropTypes.bool,
    /** 구성 요소의 내용 */
    children : PropTypes.node,
    /** 구성 요소에 적용된 스타일을 재정의 */
    classes : PropTypes.object,
    /** 루트 노드에 사용되는 구성 요소 */
    component : PropTypes.elementType,
    /** "ListItemSecondaryAction"이 마지막 자식 일 때 사용되는 컨테이너 구성 요소 */
    ContainerComponent : PropTypes.elementType,
    /**  컨테이너 구성 요소에 적용 */
    ContainerProps : PropTypes.object,
    /** true인 경우 키보드 및 마우스 입력 용으로 설계 */
    dense : PropTypes.bool,
    /** true이면 목록 항목이 비활성화 */
    disabled : PropTypes.bool,
    /** true이면 왼쪽 및 오른쪽 패딩이 제거 */
    disableGutters : PropTypes.bool,
    /** true이면 1px 밝은 테두리가 목록 항목의 맨 아래에 추가 */
    divider : PropTypes.bool,
    /** 선택한 스타일을 적용하는 데 사용 */
    selected : PropTypes.bool,
}
 
BasicListItem.defaultProps = {
    alignItems : "center",
    autoFocus : false,
    button : false,
    ContainerComponent : "li",
    ContainerProps : {},
    dense : false,
    disabled : false,
    disableGutters : false,
    divider : false,
    selected : false
} 


// export const BasicList = (props) =>{
//     return(
//         <List component="nav">
//         {
//             (props.list && props.list.length>0) &&
//             props.list.map((v,idx)=>{
//                 return <BasicListItem key={idx} icon={v.icon} text={v.text} idx={idx} btn={v.btn} 
//                 />
//             })
//         }
//         </List>
//     )
// }

// const textList = [
//     {
//         text : 'test'
//     },
//     {
//         text : 'draft'
//     }
// ]

// export const TextList = (props) =>{
//     return(
//         <BasicList list={textList}/>
//     )
// }

// const iconList = [
//     {
//         icon : InboxIcon,
//         text : 'test'
//     },
//     {
//         icon : DraftsIcon,
//         text : 'draft'
//     }
// ];

// export const IconList = (props) =>{
//     const handleClick = useCallback(()=>{
//         alert('iconListItemClick');
//     },[]);

//     return(
//         <BasicList list={iconList} click={handleClick}/>
//     )
// }

// const btnList = [
//     {
//         icon : InboxIcon,
//         text : 'test',
//         btn : ChevronRight
//     },
//     {
//         icon : DraftsIcon,
//         text : 'draft',
//         btn : ChevronRight
//     }
// ];

// export const BtnList = (props) =>{

//     const handleBtnClick = useCallback(()=>{
//         alert('btnClick');
//     },[]);

//     return(
//         <BasicList list={btnList} btnClick={handleBtnClick}/>
//     )
// }
