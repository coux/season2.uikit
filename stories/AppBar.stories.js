import React,{useCallback} from 'react';
import { AppBar,Toolbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';

import mdx from './AppBar.mdx';



export default {
    title: 'AppBar',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    root:{

    }
});

export const BasicAppBar = (props)=>{


    const classes = useStyles();


    return(
        <AppBar className={classNames([classes.root,props.styles])}  {...props}>
            <Toolbar>
            {props.children}
            </Toolbar>
        </AppBar>
    )
}


