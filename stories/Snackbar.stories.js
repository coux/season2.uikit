import React,{useCallback} from 'react';
import { Button, Snackbar, makeStyles, Container } from '@material-ui/core';
import Jam from '../assets/jam.svg';
import PropTypes from 'prop-types';
import mdx from './Snackbar.mdx';

export default {
    title: 'Snackbar',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    button : {
        borderRadius:'20px',
        background:'#000000'
    },
    text : {
        color:'#000',
        fontSize:10,
        display : 'flex'
        
    },
    root:{
        background:'#ffd400',
        borderRadius:'30px',
        boxShadow:'none',
        minWidth:'180px',
        minHeight:'40px'
    },
    back:{
        border : '1px solid #000',
        borderRadius:'30px'
    }
});

const MultilineText = (props)=>{
    const classes = useStyles();
    console.log('multiline', props)
    return(
        <div className={classes.text}>
            <div>
                <img src={props.imgFlag ? Jam: props.imgSrc } />
            </div>
            <span>
                {
                    props.jam ? 
                    <>
                        <div>연속 시청 보상</div>
                        <div>잼 {props.jam}개 받기</div>
                    </> : props.message
                    
                }
            </span>
        </div>
    )
}

export const BasicSnackbar = (props) =>{
    const classes = useStyles();

    const handleClick = useCallback(()=>{
        if(props.handleClick){
            props.handleClick();
        }
    },[]);

    return(  
        <Snackbar className={classes.back} ContentProps={{
            classes: {
                root: classes.root
            }
        }}                 
        open={props.open ? props.open : true}
        message={<MultilineText imgFlag={props.imgFlag? props.imgFlag:false} jam={props.jam? props.jam : 2} 
        message={props.message} imgSrc={props.imgSrc}/>}
        action={props.action? props.action : 
            <Button className={classes.button} color="secondary" 
            variant="contained" size="small" onClick={handleClick}>
                받기
            </Button>            
        } {...props}/>
    )
}

export const test = (props)=>{
    return(
        <BasicSnackbar open={true} anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
        }}/>
    )
}

/*

export const TextSnackBar = (props) =>{
    const onClickEvent = useCallback(()=>{
        alert("text");
    },[]);

    return(
        <BasicSnackBar img={false} jam={30} click={onClickEvent}/>
    )
}

export const ImgSnackBar = (props) =>{
    const onClickEvent = useCallback(()=>{
        alert("Img");
    },[]);

    return(
        <BasicSnackBar imgFlag={true} jam={30}/>
    )
}
*/

// Input Props 정의 
BasicSnackbar.propTypes = {
    /** 스낵바에 버튼 등 표시할 액션 내역. 메시지 뒤에 표시된다. */
    action: PropTypes.node,
    /** 스낵바의 위치 ({ horizontal: 'left' | 'right', vertical: 'top' | 'bottom' }) */
    anchorOrigin: PropTypes.object,
    /** 스낵바의  onClose 호출 전 대기하는 시간(밀리 초) */
    autoHideDuration: PropTypes.number,
    /** 스낵바에 버튼이 있을 경우 버튼의 함수 */
    handleClick: PropTypes.func,
    /** 스낵바에 표시할 이미지를 넣을지 여부 */
    imgFlag: PropTypes.bool,
    /** 스낵바에 표시할 이미지 */
    imgSrc: PropTypes.object,
    /** 잼 개수 표시할 스낵바의 경우 잼의 개수 */
    jam: PropTypes.number,
    /** 연속된 여러 스낵바를 표시할 때 각 메시지가 독립적으로 처리되도록 key를 추가하여 관리  */
    key: PropTypes.any,
    /** 스낵바에 표시되는 메시지 */
    message: PropTypes.node,
    /** 구성요소가 종료를 요청할 시 콜백 */
    onClose: PropTypes.func,
    /** 전환이 시작되기 전에 콜백이 시작 */
    onEnter: PropTypes.func,
    /** 전환이 종료되기 전에 콜백이 시작 */
    onExit: PropTypes.func,
    /** true일 경우 스낵바가 열림 */
    open: PropTypes.bool,
    /** 사용자가 스낵바를 해제하기 전에 대기하는 시간 (밀리 초) */
    resumeHideDuration: PropTypes.number,
    /** 전환에 따른 구성요소  */
    TransitionComponent: PropTypes.elementType,
    /** 전환 시간  ({ appear?: number, enter?: number, exit?: number }) */
    transitionDuration: PropTypes.number,

}

BasicSnackbar.defaultProps={
    key:0,
    open : false
}