import React, { useState,useCallback,useEffect} from 'react';
import { FormControl, Select, InputLabel, RadioGroup, FormControlLabel, Radio, FormGroup, Checkbox, Switch } from '@material-ui/core';
import PropTypes from 'prop-types';
import { ArrowDropDown } from '@material-ui/icons';
import mdx from './SelectionControls.mdx';
import { BasicTheme } from './BasicTheme.stories';


export default {
    title: '/selectionControls/test',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const exampleList = [
    {label : 'option1', value : 1},
    { label: 'option2', value: 2 },
    { label: 'option3', value: 3 }
];

export const BasicSelectBox = (props) =>{

    const [value, setValue] = useState('');
    const [list, setList] = useState((props.list && props.list.length > 0) ? props.list : exampleList);
    const handleChange = useCallback((e)=>{
        setValue(e.target.value);
    },[]);

    return(
        <BasicTheme>
            <FormControl variant="outlined">
                <InputLabel >Label</InputLabel>
                <Select
                    native
                    value={value}
                    onChange={handleChange}
                    label="Selected">
                    <option value="" />
                    {
                        list.map((v, idx) => {
                            return (
                                <option value={v.value} key={idx}>{v.label}</option>
                            )
                        })
                    }
                </Select>
            </FormControl>
        </BasicTheme>
    )
}

// 셀렉트박스
export const SelectBox = (props) => {
    const [state, setState] = useState({
        selected: '',
        name: '',
    });
    const [list, setList] = useState((props.list && props.list.length > 0) ? props.list : exampleList);

    const handleChange = (event) => {
        const name = event.target.name;
        setState({
            ...state,
            [name]: event.target.value,
        });
    };

    return (
        <FormControl variant="outlined">
            <InputLabel >Label</InputLabel>
            <Select
                native
                value={state.selected}
                onChange={handleChange}
                label="Selected">
                <option value="" />
                {
                    list.map((v, idx) => {
                        return (
                            <option value={v.value} key={idx}>{v.label}</option>
                        )
                    })
                }
            </Select>
        </FormControl>
    )
}

export const BasicRadioBtn = (props) =>{
    const [value,setValue] = useState('');

    const handleChange = useCallback((e)=>{
        setValue(e.target.value);
        console.log('select', e.target.value);
    },[]);

    return(
        <FormControl>
            <RadioGroup defaultValue={props.defaultValue} onChange={handleChange}>
                {
                    props.list && 
                    props.list.map((v,idx)=>{
                        return(
                            <FormControlLabel value={v.value} key={idx}
                            control={<Radio color="primary" />}
                            label={v.label} labelPlacement="bottom"/>
                        )
                    })
                }
                
            </RadioGroup>
        </FormControl>
    )
}

const radioList = [    
    {label:'popkon1', value:'popkon1'},
    {label:'popkon2', value:'popkon2'},
    {label:'popkon3', value:'popkon3'}
]

export const RadioBtnGroupEx = (props) =>{
    return(
        <BasicRadioBtn list={radioList} defaultValue='popkon2'/>
    )
}

const checkList = [    
    {label:'popkon1', checked:false},
    {label:'popkon2', checked:false},
    {label:'popkon3', checked:true}
]

export const BasicCheckbox = (props) =>{
    const [value,setValue] = useState('');

    const handleChange = useCallback((e)=>{
        setValue(props.list.map((v)=>value.label===e.target.label ? {...v, checked: e.target.checked}: v));
        console.log('value',value);
    },[]);

    return(
        <FormControl component="fieldset">
            <FormGroup row>
                {
                    props.list && 
                    props.list.map((v,idx)=>{
                        console.log('ch v',v);
                        return(
                            <FormControlLabel key={idx} onChange={handleChange}
                            control={<Checkbox color="primary" checked={v.checked} name={v.label}/>}
                            label={v.label} labelPlacement="bottom"/>
                        )
                    })
                }
            </FormGroup>
        </FormControl>
    )
}

export const CheckboxGroupEx = (props) =>{
    return(
        <BasicCheckbox list={checkList} />
    )
}

BasicSelectBox.propTypes = {
    /** true일 경우 팝오버의 너비가 자동으로 설정되고, 그렇지 않으면 최소한의 너비가 설정 */
    autoWidth: PropTypes.bool,
    /** 기본으로 표현되는 값  */
    defaultValue: PropTypes.any,
    /** true이면 항목을 선택하지 않아도 값이 표시 */
    displayEmpty: PropTypes.any,
    /** 화살표를 표시하는 아이콘 */
    IconComponent: PropTypes.elementType,
    /** 셀렉트에 적용되는 어트리뷰트 */
    inputProps: PropTypes.object,
    /** 셀렉트 창의 라벨 */
    label: PropTypes.node,
    /** true일 경우 복수 선택할 수 있음  */
    multiple: PropTypes.bool,
    /** 셀렉트 박스 선택 시에 호출되는 함수  */
    onChange: PropTypes.func,
    /** 셀렉트 박스를 열었을때 호출되는 함수 */
    onOpen: PropTypes.func,
    /** 셀렉트 박스가 열렸을때의 상태를 제어*/
    open: PropTypes.bool,
    /** 선택한 값을 랜더링  */
    renderValue: PropTypes.func,
    /** 입력 값 */
    value: PropTypes.any,
    /** 커스터마이징 스타일 ({ 'filled'| 'outlined'| 'standard' }) */
    variant: PropTypes.string,
}

BasicSelectBox.defaultProps={
    autoWidth:false,
    displayEmpty : false, 
    IconComponent: ArrowDropDown,
    multiple : false,
    label : 'label',
    variant:"outlined"
}