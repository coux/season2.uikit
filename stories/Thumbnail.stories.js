import React from 'react';
import { Card, CardMedia, CardActionArea, Badge } from '@material-ui/core';
import { Settings } from '@material-ui/icons';
import {makeStyles,withStyles} from '@material-ui/core/styles';


export default{
    title : 'Thumbnail'
}

const useStyles = makeStyles({
   root : {
       width : 275,
       height : 275
   },
   media : {
       maxWidth: 275,
        height: 275
   }
});

export const BasicThumbnail = (props)=>{
    const classes = useStyles();

    return(
        <>
        <Badge anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
        }}>
            <Card variant="outlined" className={classes.root}>
                <CardActionArea>
                    
                    <CardMedia
                        image = "http://via.placeholder.com/350x150"
                        className = {classes.media}/>
                </CardActionArea>
                
            </Card>
            </Badge>
        </>
        
    )
}