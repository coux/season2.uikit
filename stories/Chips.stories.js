import React,{useCallback} from 'react';
import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';
import mdx from './Chips.mdx';

export default {
    title: 'Chips',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    root:{
        background:'#c5c8d5',
        color:'#ffffff'
    }
});

export const BasicChip = (props)=>{
    /**
     * 해시태그 관련된 기본 ui 컴포넌트
     *  prop ->
     * label: 칩의 라벨(해시태그)
     * value: 칩의 값(해시태그 인덱스 등 넘겨줄 값)
     * delete : 칩(해시태그)가 편집모드인 경우에만 생기는 아이콘으로 delete값이 있을때만 표시. delete값은 함수
     * click : 칩을 클릭 시 적용되는 함수. 클릭이 있을 경우만 표시 
     */

    const classes = useStyles();

    const handleDelete = useCallback(() => {
        console.log('delete');
        props.delete();
    },[]);

    const handleClick = useCallback(()=>{
        console.log('click');
        props.click();
    },[]);

    return(
        <Chip className={classNames([classes.root,props.styles])} label={props.label&& `#${props.label}`} value={props.value} 
        onDelete={props.delete && handleDelete}
        onClick={props.click&& handleClick}/>
    )
}

// Chip Props 정의 
BasicChip.propTypes = {
    /** 아바타 요소 */
    avatar : PropTypes.element,
    /** 자식 구조를 변경해야하는 경우 구성 요소 */
    children : PropTypes.unsupportedProp,
    /** 구성 요소에 적용된 스타일을 재정의 */
    classes : PropTypes.object,
    /** 클릭 가능 경우 ture: 클릭 가능 / false: 클릭 불가능 (true | false) */
    clickable : PropTypes.bool,
    /** 구성 요소의 색상 */
    color : PropTypes.default,
    /** 루트 노드에 사용되는 구성 요소 */
    component : PropTypes.elementType,
    /** 기본 삭제 아이콘  */
    deleteIcon : PropTypes.element,
    /** true이면 chip이 비활성화 된 상태로 표시 (true | false) */
    disabled : PropTypes.bool,
    /** 아이콘 요소 */
    icon : PropTypes.element,
    /** chip의 라벨(해시태그) */
    label : PropTypes.node,
    /** 삭제 클릭하면 콜백 함수 */
    onDelete : PropTypes.func,
    /** chip의 크기 */
    size : PropTypes.medium,
    /** default/outlined */
    variant : PropTypes.default
}

BasicChip.defaultProps = {
    color : "default",
    disabled : false,
    size : "medium",
    variant : "default"
}



// Chip Props 정의 
// export const ViewChips = (props) =>{
//     const onClick = useCallback(()=>{
//         alert('click');
//     },[]);

//     return(
//         <BasicChip label="test" click={onClick} {...props}/>
//     )
// }

// export const EditChips = (props) =>{
//     const onDelete = useCallback(()=>{
//         alert('delete');
//     },[]);
//     return(
//         <BasicChip label="test" delete={onDelete} {...props}/>
//     )
// }

