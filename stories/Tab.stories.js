import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Tab } from '@material-ui/core';
import mdx from './Tab.mdx';

export default{
    title: '/tabs/Tab',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

const useStyles = makeStyles({
    listTab: {
        border: 'solid 1px #dae1e6',
        background: '#f4f4f8'
    },
    listSelected: {
        background: '#fff'
    },
    listIndicator: {
        background: 'none'
    },
    root : {
        ["@media (min-width:600px)"]: { minWidth : '0px' }
    }
});

export const BasicTab = (props) =>{
    const classes = useStyles();
    const styles = makeStyles(theme => ({ prop: props.styles }));

    return (
        <Tab icon={props.children} label={props.label} value={props.value}
            classes={{ root: classes.root,  }}  
            className={classNames(styles().prop)}
            {...props} />
    )
}
// Tab Props 정의
BasicTab.propTypes = {
    /** 탭에서 현재 선택 된 값 */
    value: PropTypes.any,
    /** 각 탭에 붙여진 이름 */
    label: PropTypes.node,
    /** 탭에 아이콘이 들어가는 경우 지정해주는 프로퍼티*/
    icon: PropTypes.node,
    /** 해당 속성을 true로 줄 경우 해당 탭이 비활성화 됨 */
    disabled: PropTypes.bool,
    /** 탭 라벨이 단일행으로 나타내도록 표시. 필요하면 두번째 행까지 사용 가능하다.*/
    wrapped: PropTypes.bool,
    /** 사용자가 커스터마이징하게 줄 수 있는 탭 스타일 */
    styles: PropTypes.object,
};

BasicTab.defaultProps = {
    disabled : false,
    wrapped: false
}

