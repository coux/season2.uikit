import React,{useCallback} from 'react';
import {Snackbar,Button} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';
import MuiAlert from '@material-ui/lab/Alert';
import { SnackbarProvider,useSnackbar,SnackbarContent   } from 'notistack';

import mdx from './Toast.mdx';


export default {
    title: 'Toast',
     parameters: {
         docs: {
             page: mdx
         }
     }
};

const useStyles = makeStyles({
    root:{

    }
});

export const BasicToastMessage = React.forwardRef((props, ref) => {
    const classes = useStyles();


    return (
         <SnackbarContent ref={ref} className={classes.root}>
            {props.message}
        </SnackbarContent>
    );
});


function MyApp() {
  const { enqueueSnackbar } = useSnackbar();
  const handleClick = () => {
    enqueueSnackbar('I love snacks.');
  };
  const handleClickVariant = (variant) => () => {
    // variant could be success, error, warning, info, or default
    enqueueSnackbar('This is a success message!', { variant });
  };
  return (
    <React.Fragment>
      <Button onClick={handleClick}>Show snackbar</Button>
      <Button onClick={handleClickVariant('success')}>Show success snackbar</Button>
    </React.Fragment>
  );
}

export  function ToastEx(){
    return (
         <SnackbarProvider
            content={(key, message) => (
               <BasicToastMessage id={key} message={message} />
           )}>
            <MyApp />
        </SnackbarProvider>
    )
}
