import React,{useState,useEffect, useCallback} from 'react';
import { Snackbar, Slide } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Goldenbell from '../assets/goldenbell.svg';
import Note from '../assets/note.svg';
import MEGA from '../assets/megaphone.svg';

export default {
    title: 'Banner',
};


const useStyles = makeStyles({
    text:{
        color : '#fff',
        '& span':{
            fontWeight : 'bold'
        }
    },
    nickname : {
        color: '#fff568'
    },
    root : {
        borderRadius:'32px',
        '@media(minWidth: 780px)' : {
            borderRadius:'0'   
        }
    },
    radius : {
        borderRadius:'32'
    },
    img : {
        verticalAlign:'middle'
    },
    goldenbell:{
        background:'#05c3a7',
    },
    crew:{
        background:'#6f01ff'
    },
    mega:{
        background:'#dd3518'
    }
});

const NameLabel = (({nickname})=>{
    const classes = useStyles();
    return(
        <span className={classes.nickname}>{nickname}</span>
    )
})

const AlarmText = ({nickname, alarmType,value,img})=>{
    const classes = useStyles();

    return (
        <div className={classes.text}>
            <img src={img} className={classes.img}/>
            <NameLabel nickname={nickname}/>
            {alarmType.decoText && alarmType.decoText}
            <span>
                {value ? value: alarmType.valueText}
            </span>
            {alarmType.descText}
        </div>
    )
}

const ALARMTEXT = {
    GOLDENBELL:{decoText:"님이 ", descText:"을 후원하셨습니다.", valueText:"골든벨 팝콘"},
    LEVEL:{decoText:"님이 방송자 레벨 ", descText:"을 달성하였습니다."},
    CREW:{descText:" 크루가 생성되었습니다. 크루를 응원해보세요!"},
    CHIEF:{decoText:"님이 ", descText:"님의 회장 등극하셨습니다."},  
    POPKON:{decoText:"님이 ", descText:" 팝콘을 후원받았습니다."},
    MEGAPHONE : {descText:" 메가폰을 날려본다! 어떠냐 다들 우헤헤헤헤~"},
};



export const BasicBanner = (props) =>{
    /**
     * 메인의 한줄안내와 관련된 기본 ui 컴포넌트
     * 통합기획서 27p에 한줄안내에 정의되어있음
     * alarmType : 한줄안내에 정의되어있는 타입 중 하나만 골라서 값을 주면 됨. 각각 GOLDENBELL/LEVEL/CREW/CHIEF/POPKON
     * direction: 한줄안내가 슬라이딩 되는 방향 
     * nickname: 한줄안내 시에 닉네임 정의
     * value: 한줄 안내시의 크루명, 팝콘 개수 등
     * open: 배너가 열리는지 여부 true/false
     */
    const classes = useStyles();
    const [alarmType, setAlarmType] = useState("");
    const [direction, setDirection] = useState(props.direction);

    
    useEffect(() => {
        if(props && props.alarmType){
            switch(props.alarmType){
                case "GOLDENBELL":
                    setAlarmType(ALARMTEXT.GOLDENBELL);
                break;
                case "LEVEL":
                    setAlarmType(ALARMTEXT.LEVEL);
                break;
                case "CREW":
                    setAlarmType(ALARMTEXT.CREW);
                break;
                case "CHIEF":
                    setAlarmType(ALARMTEXT.CHIEF);
                break;
                case "POPKON":
                    setAlarmType(ALARMTEXT.POPKON);
                case "MEGAPHONE":
                    setAlarmType(ALARMTEXT.MEGAPHONE);
                break;
            }
        }   
    }, [props.alarmType]);

    const SlideTransition = (prop) => <Slide {...prop}  direction={direction}/>
    
    return(
        <Snackbar ContentProps={{
            classes: {
                root: classNames([classes.root, props.styles]) 
            }
        }} open={props.open} message={<AlarmText nickname={props.nickname} value={props.value} img={props.img} alarmType={alarmType}/>}
        TransitionComponent={SlideTransition}/>
     
    )
}

export const GoldenbellBanner = (props) =>{
    const classes = useStyles();
    return(
        <BasicBanner {...props}
            nickname="꼬깔콘은매운맛" alarmType="GOLDENBELL" direction="right" 
            styles={classes.goldenbell} open={true} img={Goldenbell}
        />
    )
}

export const LevelBanner = (props) =>{
    return(
        <BasicBanner {...props}
            nickname="꼬깔콘은매운맛" alarmType="LEVEL" value="20" direction="left" open={true}
        />
    )
}

export const CrewBanner= (props) =>{
    const classes = useStyles();
    return(
        <BasicBanner {...props}
            nickname="꼬깔콘은매운맛" alarmType="CREW" direction="up" 
            styles={classes.crew} open={true} img={Note}
        />
    )
}

export const ChiefBanner = (props) =>{
    return(
        <BasicBanner {...props}
            nickname="꼬깔콘은매운맛" alarmType="CHIEF" value="옥수수" direction="down" open={true}
        />
    )
}

export const PopkonBanner = (props) =>{
    return(
        <BasicBanner {...props}
            nickname="꼬깔콘은매운맛" alarmType="POPKON" value="50" direction="down" open={true}
        />
    )
}

export const MegaphoneBanner = (props) =>{
    const classes = useStyles();
    return(
        <BasicBanner
            nickname="꼬깔콘은매운맛" alarmType="MEGAPHONE" 
            direction="down" open={true} img={MEGA} styles={classes.mega} {...props}
        />
    )
}

