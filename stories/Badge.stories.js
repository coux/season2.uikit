import React from 'react';
import { Badge, makeStyles, Avatar } from '@material-ui/core';
import classNames from 'classnames';
import mdx from './Badge.mdx'
import PropTypes, { node } from 'prop-types';

export default{
    title : 'Badge',
    parameters : {
        docs : {
            page : mdx
        }
    }
}

const useStyles = makeStyles({
    badge: {
        backgroundColor : props => props.bgColor,
        color: props => props.textColor,
        borderRadius: '2px',
        transform: 'scale(1) translate(0, 0)'
    }
});

export const BasicBadge = (props) =>{
    const classes = useStyles(props);
    return(
        <Badge
            classes={{
                badge: classes.badge, // badge default style
            }}
            {...props}
        />
    )
}

// Badge Props 정의
BasicBadge.propTypes = {
    /** 배지의 위치 ({ horizontal: 'left' | 'right', vertical: 'top' | 'bottom' }) */
    anchorOrigin: PropTypes.object,
    /** 배지 내에서 렌더링 된 컨텐츠 node 형태로 입력 가능*/
    badgeContent: PropTypes.node,
    /** 해당 노드를 기준으로 배지가 추가 */
    children: PropTypes.node,
    /** false 일 때 노출 (true | false) */
    invisible: PropTypes.bool,
    /** 최대 카운트 수 */
    max: PropTypes.number,
    /** 배지에 내용이 없을 때 노출 여부 */
    showZero: PropTypes.bool,
    /** 배지를 점의 형태로 노출 */
    variant: PropTypes.string,
    /** 배지의 배경색 변경 ( 컬러헥사코드 및 rgb값 입력 ex:'#FFFFFF' | rgb(255,255,255) ) */
    bgColor: PropTypes.string,
    /** 배지의 글자색 변경 ( 컬러헥사코드 및 rgb값 입력 ex:'#FFFFFF' | rgb(255,255,255) ) */
    textColor: PropTypes.string,
};

// Badge Props Default
BasicBadge.defaultProps = {
    anchorOrigin: { horizontal: 'left', vertical: 'top' },
    badgeContent: 'badge',
    invisible: false,
    bgColor: '#dae1e6',
    textColor: '#222',
    variant: 'standard'
};