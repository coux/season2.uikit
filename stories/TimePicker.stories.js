import React,{useCallback} from 'react';
import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import classNames from 'classnames';


import koLocale from "date-fns/locale/ko";

import {
  MuiPickersUtilsProvider,
  TimePicker,
} from '@material-ui/pickers';

import 'date-fns';

import DateFnsUtils from '@date-io/date-fns';

import mdx from './TimePicker.mdx';



export default {
    title: 'TimePicker',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    root:{

    }
});


export const BasicTimePicker = (props)=>{

    const classes = useStyles();

    return(
         <MuiPickersUtilsProvider utils={DateFnsUtils} locale={koLocale}>
            <TimePicker
                 okLabel='확인'
                 cancelLabel='취소'
            className={classNames([classes.root,props.styles])}  {...props} />
        </MuiPickersUtilsProvider>
    )
}

