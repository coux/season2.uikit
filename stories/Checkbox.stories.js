import { Checkbox, FormControl, FormControlLabel, FormGroup } from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import mdx from './Checkbox.mdx';

export default {
    title: '/selectionControls/Checkbox',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

const exampleCheckList = [
    { label: 'popkon1', checked: false },
    { label: 'popkon2', checked: false },
    { label: 'popkon3', checked: true }
]

export const BasicCheckbox = (props) =>{
    const [value, setValue] = useState('');
    const [list, setList] = useState((props.list && props.list.length > 0) ? props.list : exampleCheckList);

    const handleChange = useCallback((e) => {
        setValue(props.list.map((v) => value.label === e.target.label ? { ...v, checked: e.target.checked } : v));
        console.log('value', value);
    }, []);

    return (
        <FormControl component="fieldset">
            <FormGroup row>
                {
                    list &&
                    list.map((v, idx) => {
                        console.log('ch v', v);
                        return (
                            <FormControlLabel key={idx} onChange={handleChange}
                                control={<Checkbox color="primary" checked={v.checked} name={v.label} />}
                                label={v.label} labelPlacement="bottom" />
                        )
                    })
                }
            </FormGroup>
        </FormControl>
    )
}

// Checkbox Props 정의
BasicCheckbox.propTypes = {
    /** true일 경우 컴포넌트는 체크된 상태 */
    checked: PropTypes.bool,
    /** 체크된 상태일때의 표현하는 아이콘 */
    checkedIcon: PropTypes.node,
    /** 컴포넌트의 색상 ({ 'default' | 'primary' | 'secondary' }) */
    color: PropTypes.string,
    /** true일 경우 스위치가 비활성화 */
    disabled: PropTypes.bool,
    /** 상태가 변경되면 콜백하는 함수 */
    onChange: PropTypes.func,
    /** true일 경우 input요소가 필요함  */
    required: PropTypes.bool,
    /** 체크박스의 크기  ({ 'medium'| 'small' }) */
    size: PropTypes.string,
    /** 구성요소의 값 */
    value: PropTypes.any,
}

BasicCheckbox.defaultProps = {
    size: 'medium'
}