import React from 'react';
import { CircularProgress, makeStyles } from '@material-ui/core';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import mdx from './Indicator.mdx';

export default {
    title : 'Indicator',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

const useStyles = makeStyles({
    root: {
        height : '7px',
        borderRadius : 10,
        background : '#dae1e6'
    },
    bar : {
        background : '#ffcf20'
    },
    round: {
        position: 'relative'
    },
    progress : {
        left : 0,
        position: 'absolute',
    },
    bottom:{
        color: 'lightgrey'
    }
});

export const BasicRoundIndicator = (props) => {
    const classes = useStyles();
    return(
        <CircularProgress loading="true" size={50} thickness={4} {...props}/>
    )
}


// BasicRoundIndicator Props 정의 
BasicRoundIndicator.propTypes = {
    /** 구성 요소에 적용된 스타일을 재정의 */
    classes : PropTypes.object,
    /** 구성 요소의 색상 */
    color : PropTypes.string,
    /** true이면 애니메이션이 비활성화 */
    disableShrink : PropTypes.bool,
    /** 원의 크기값 */
    size : PropTypes.number,
    /** 원의 두께 */
    thickness : PropTypes.number,
    /** 진행률 (0에서 100 사이의 값)*/
    value : PropTypes.number,
    /** 사용할 변형. 진행 값이없는 경우 indeterminate를 사용 */
    variant : PropTypes.string
}

BasicRoundIndicator.defaultProps = {
    color : "primary",
    disableShrink : false,
    size : 40,
    thickness: 3.6,
    value : 0,
    variant : "indeterminate"
}
  


// export const BasicRoundIndicator = (props) =>{
//     const classes = useStyles();

//     return(
//         <div className={classes.round}>
//             <Fade
//             style={{
//                 transitionDelay: props.loading ? '800ms' : '0ms',
//               }}
//             in={props.loading }
//             unmountOnExit>
//                 <div>
//                     <CircularProgress className={classes.bottom} size={props.size}
//                     variant="determinate" thickness={props.thickness} value={100}/>
//                     <CircularProgress className={classNames([classes.progress,props.styles])} size={props.size} color="primary"
//                     variant="indeterminate" thickness={props.thickness} />
//                 </div>    
//             </Fade>
//         </div>
//     )
// }
// export const RoundIndicatorEx = (props) =>{
//     return(
//         <BasicRoundIndicator loading={true} size={50} thickness={4} {...props}/>
//     )
// }