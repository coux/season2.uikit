import { ThemeProvider } from '@material-ui/core';
import React from 'react';
import { muiTheme } from '../stories/stylessheet.js';

export const BasicTheme = (props) => {
    return (
        <ThemeProvider theme={muiTheme}>{props.children}</ThemeProvider>
    )
}