import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import { CheckBox } from '@material-ui/icons';
import React from 'react';

export default{
    title : 'Table'
}

export const BasicTable = (props) =>{
    return(
        <Table {...props} />
    )
}

const list = [
    {
        name : '크루 구독',
        date : '2020.09.09',
        money : 30000,
        duration : '2020.08.09~2020.10.10'
    },
    {
        name : '크루 구독',
        date : '2020.09.09',
        money : 20000,
        duration : '2020.08.09~2020.10.10'
    },
    {
        name : '크루 구독',
        date : '2020.09.09',
        money : 100000,
        duration : '2020.08.09~2020.10.10'
    }
]

export const SimpleTable = (props) =>{

    return(
        <BasicTable>
            <TableHead>
            <TableRow>
                <TableCell>상품명</TableCell>
                <TableCell >결제일</TableCell>
                <TableCell>구독기간</TableCell>
                <TableCell>결제금액</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                {
                    list.map((v,idx)=>(
                        <TableRow key={idx}>
                            <TableCell>{v.name}</TableCell>
                            <TableCell>{v.date}</TableCell>
                            <TableCell>{v.duration}</TableCell>
                            <TableCell>{v.money}</TableCell>
                        </TableRow>
                    ))
                }
            </TableBody>
        </BasicTable>
        
    )
}

export const CheckboxTable = (props) =>{
    return(
        <BasicTable>
            <TableHead>
            <TableRow>
                <TableCell>
                    <CheckBox checked={false} />
                    상품명</TableCell>
                <TableCell >결제일</TableCell>
                <TableCell>구독기간</TableCell>
                <TableCell>결제금액</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                {
                    list.map((v,idx)=>(
                        <TableRow key={idx}>
                            <TableCell>{v.name}</TableCell>
                            <TableCell>{v.date}</TableCell>
                            <TableCell>{v.duration}</TableCell>
                            <TableCell>{v.money}</TableCell>
                        </TableRow>
                    ))
                }
            </TableBody>
        </BasicTable>
    )
}
/*
export const SelectTable = (props) =>{
    return(
        <BasicTabel>
            <TableHead>
            <TableRow>
                <TableCell>상품명</TableCell>
                <TableCell >결제일</TableCell>
                <TableCell>구독기간</TableCell>
                <TableCell>결제금액</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                {
                    list.map((v,idx)=>(
                        <TableRow key={idx}>
                            <TableCell>{v.name}</TableCell>
                            <TableCell>{v.date}</TableCell>
                            <TableCell>{v.duration}</TableCell>
                            <TableCell>{v.money}</TableCell>
                        </TableRow>
                    ))
                }
            </TableBody>
        </BasicTabel>
    )
}

export const SortedTable = (props) =>{
    return(
        <BasicTabel>
            <TableHead>
            <TableRow>
                <TableCell>상품명</TableCell>
                <TableCell >결제일</TableCell>
                <TableCell>구독기간</TableCell>
                <TableCell sortDirection={false} >결제금액</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                {
                    list.map((v,idx)=>(
                        <TableRow key={idx}>
                            <TableCell>{v.name}</TableCell>
                            <TableCell>{v.date}</TableCell>
                            <TableCell>{v.duration}</TableCell>
                            <TableCell>{v.money}</TableCell>
                        </TableRow>
                    ))
                }
            </TableBody>
        </BasicTabel>
    )
}
*/