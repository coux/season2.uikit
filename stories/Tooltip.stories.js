import React,{useState,useEffect} from 'react';
import { Tooltip,Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes, { node } from 'prop-types';
import mdx from './Tooltip.mdx';

export default {
    title: 'Tooltip',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    arrow : {
        color:'#000000'
    },
    tooltip:{
        backgroundColor: '#000000',
    }
});

export const BasicTooltip = (props) =>{
    const classes = useStyles();

    return(
        <Tooltip title="test" arrow classes={{
            tooltip:classes.tooltip,
            arrow : classes.arrow
        }}
        {...props}>
        {
            props.children ? props.children : <Button>test</Button>
        }
        </Tooltip>
    )
}

// Tooltip Props 정의
BasicTooltip.propTypes = { 
    /** 툴팁 제목 */
    title: PropTypes.string,
    /** 툴팁에 붙어있는 화살표 */
    arrow: PropTypes.bool,
    /**  툴팁 위치 ({'bottom-end'| 'bottom-start'| 'bottom'| 'left-end'| 'left-start'| 'left'| 'right-end'| 'right-start'| 'right'| 'top-end'| 'top-start'| 'top'})*/
    placement: PropTypes.string,
};

// Tooltip Props Default
BasicTooltip.defaultProps = {
    title : 'test',
    arrow : true,
    placement : 'bottom'
}