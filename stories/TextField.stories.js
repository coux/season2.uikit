import React, { useCallback, useState } from 'react';
import { TextField, InputAdornment, IconButton } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import PropTypes from 'prop-types';
import mdx from './TextField.mdx';
import { BasicTheme } from './BasicTheme.stories';
import { makeStyles } from '@material-ui/styles';

export default {
    title: '/TextFields/BasicTextField',
    parameters: {
        docs: {
            page: mdx
        }
    }
};

const useStyles = makeStyles({
    input: {
        background: '#f7f9fa',
        '&::placeholder': {
            textOverflow: 'ellipsis !important',
            color: 'blue'
        }
    }
});


export const BasicInput = (props)=>{
    const [value, setValue] = useState('');
    const classes = useStyles();
    const handleChange = useCallback((e)=>{
        setValue(e.target.value);
        if(props.handleChange){
            props.handleChange();
        }
    },[]);

    return(
        <BasicTheme>
            <TextField value={value} onChange={handleChange}
                label={props.label? props.label: 'label'}
                defaultValue={props.defaultValue}
                color={props.color}
                variant= 'outlined'
                error={props.error}
                helperText={props.helperText}
                InputProps={{className:classes.input}}
                placeholder="test"
                {...props}
            />
        </BasicTheme>        
    )
}

// Input Props 정의 
BasicInput.propTypes = {
    /** 자동완성 기능으로 사용자가 폼을 채울때 더 빠르게 채우도록 해준다.  */
    autoComplete: PropTypes.bool,
    /** 초기 화면이 보여질때 해당 프로퍼티가 true인 인풋에 포커싱이 간다. */
    autoFocus: PropTypes.bool,
    /** 컴포넌트 색상으로  적합한 테마 색상을 지원한다. 'primary'| 'secondary' */
    color: PropTypes.string,
    /** 인풋의 기본값 */
    defaultValue: PropTypes.any,
    /** 인풋을 사용할지 여부 */
    disabled: PropTypes.bool,
    /** 레이블이 오류 상태로 표시되는지 체크하는 여부 */
    error: PropTypes.bool,
    /** 인풋이 컨테이너의 전체 너비를 차지하는지 여부 */
    fullWidth: PropTypes.bool,
    /** 도우미 텍스트*/
    helperText: PropTypes.node,
    /** InputLabel 요소에 적용되는 프로퍼티 */
    InputLabelProps: PropTypes.object,
    /** input요소에 벅용되는 프로퍼티 FilledInput, OutlinedInput, Input 등이 적용된다. */
    InputProps: PropTypes.object,
    /** input 컨텐트의 라벨 */
    label: PropTypes.node,
    /** 구성요소의 수직간격을 조절  ({ 'dense'| 'none'| 'normal' }) */
    margin: PropTypes.string,
    /** true일 경우 input대신 textarea가 랜더링 */
    multiline: PropTypes.bool,
    /** 인풋 엘리먼트의 네임어트리뷰트 */
    name: PropTypes.string,
    /** 인풋 값이 변경될때 호출되는 함수 */
    onChange: PropTypes.func,
    /** 사용자가 값을 입력하기 전 인풋에 표시됨 */
    placeholder: PropTypes.string,
    /** 해당 프로퍼티가 true일 경우 해당 인풋에는 값이 무조건 있어야함 */
    required: PropTypes.bool,
    /** 멀티라인인 경우 표시되는 row 수 */
    rows: PropTypes.number,
    /** 멀티라인인 경우 표시되는 row 최대수 */
    rowsMax: PropTypes.number,
    /** 텍스트 필드의 크기 ({ 'medium'| 'small' })  */
    size: PropTypes.string,
    /** 초기 화면이 보여질때 해당 프로퍼티가 true인 인풋에 포커싱이 간다. */
    value: PropTypes.any,
    /** 사용자가 줄 수 있는 각각의 스타일 ({ 'filled'| 'outlined'| 'standard' })  */
    variant: PropTypes.string,
}

BasicInput.defaultProps = {
    autoFocus:false,
    color:'primary',
    disabled:false,
    error:false,
    fullWidth:false,
    multiline: false,
    required:false,
    label: 'label',
    color:'primary',
    variant: 'outlined'
}