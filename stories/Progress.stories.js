import React from 'react';
import { LinearProgress, makeStyles } from '@material-ui/core';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import mdx from './Progress.mdx';

export default {
    title : 'Progress',
    parameters: {
        docs: {
            page: mdx
        }
    }
}

const useStyles = makeStyles({
    root: {
        height : '7px',
        borderRadius : 10,
        background : '#dae1e6'
    },
    bar : {
        background : '#ffcf20'
    },
    round: {
        position: 'relative'
    },
    progress : {
        left : 0,
        position: 'absolute',
    },
    bottom:{
        color: 'lightgrey'
    }
});


export const BasicProgress = (props) =>{
    const classes = useStyles();

    return(
        <LinearProgress value={props.value} variant={props.variant} classes={{root:classNames([classes.root,props.styles]), bar : classes.bar}}/>
    )
}

// BasicProgress Props 정의 
BasicProgress.propTypes = {
    /** 구성 요소에 적용된 스타일을 재정의 */
    classes : PropTypes.object,
    /** 구성 요소의 색상 */
    color : PropTypes.string,
    /** 진행률 (0에서 100 사이의 값) */
    value : PropTypes.number,
    /** 버퍼 변형의 값 (0에서 100 사이의 값) */
    valueBuffer : PropTypes.number,
    /**  진행 값이 없을 때 indeterminate 또는 query를 사용 */
    variant : PropTypes.string
}

BasicProgress.defaultProps = {
    color : "primary",
    variant : "indeterminate"
}