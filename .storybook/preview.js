// .storybook/preview.js

import React from 'react';

import { addDecorator } from '@storybook/react';
import { ThemeProvider } from '@material-ui/core/styles';

import { muiTheme } from '../stories/stylessheet.js';

addDecorator((story) => 
    <ThemeProvider theme={muiTheme}>{story()}</ThemeProvider>
);